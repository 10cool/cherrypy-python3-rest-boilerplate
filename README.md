# CherryPy Python3 restful api boilerplate
Quick project to clone off of for making small restful server

## Setup
```
virtualenv -p python3 venv
source venv/bin/active
pip install -r requirements.txt
python app.py
```

## Testing
```
source venv/bin/active
cd tests/
pytest .
```
