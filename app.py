import cherrypy

from web.handlers.root import Root

app = Root()

config = {
    '/': {
        'request.dispatch': cherrypy.dispatch.MethodDispatcher(),
        'tools.json_in.on': True,
        'tools.json_out.on': True
    }
}

cherrypy.quickstart(app, config=config)
