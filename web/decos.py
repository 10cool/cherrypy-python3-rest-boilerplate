
def safe_app_handler(config):
    """Wraps your API end points in automatic config dirven error handling
    The main purpose here is for your appliction logic to raise your own
    custom Exception classes that you know ar safe for the appliction. For
    example an API should return 404 only if a resource doesn't exist, but
    typically you would just want to return some error (or nothing) in a
    standard format at a 200.

    Useage as follows:

    class DummyException(Exception):
        pass


    def dummy_call_back(e):
        return "You getting this message and 200, not 500"


    class ApiRoot(object):

        @decos.safe_app_handler({DummyException: dummy_call_back})
        def GET(self, a, b, c):
            # This will return the string and thus a 200 response
            # instead of the exception not being handled and returning
            # a 500 error
            raise DummyException
    """

    def real_decorator(func):

        def wrapper(*args, **kwargs):
            try:
                return func(*args, **kwargs)
            except Exception as e:
                if config:
                    te = type(e)
                    if te in config:
                        return config[te](e)

                raise

        return wrapper

    return real_decorator
