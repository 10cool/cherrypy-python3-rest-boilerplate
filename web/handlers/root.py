from web import decos


class Root(object):
    exposed = True

    @decos.safe_app_handler(None)
    def GET(self):
        return self.response_creator()

    def response_creator(self):
        return 'OK'
