import sys
import unittest
sys.path.append("../")

from web.handlers.root import Root

class TestRoot(unittest.TestCase):

    def test_response_is_ok(self):
        root = Root()
        self.assertEqual('OK', root.response_creator())
